package secret

import (
	"fmt"
	"net/http"

	"local.com/roadmap-JWT/internal/user"
)

func SecretFunc(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context().Value(user.UserIDKey).(*user.JWTClaims)
	userID := ctx.UserID

	fmt.Fprintf(w, "This is a secret of userID %d and you need authenticate to view it", userID)
}
