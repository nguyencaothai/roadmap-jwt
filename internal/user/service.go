package user

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
	"local.com/roadmap-JWT/config"
)

func checkValidUser(userInput UserInput) (string, error) {
	var user User
	user, errQuery := queryUser(userInput)
	if errQuery != nil {
		if errQuery == sql.ErrNoRows {
			return "", errors.New("unauthorized")
		}

		return "", errors.New("internal error")
	}

	if ok := checkHashPassword(userInput.Password, user.Password); ok {
		jwt, errGenerate := generateJWT(user.ID)
		if errGenerate != nil {
			return "", errors.New("internal error")
		}

		return jwt, nil
	}

	return "", errors.New("unauthorized")
}

func checkHashPassword(passwordInput, hash string) bool {
	errHash := bcrypt.CompareHashAndPassword([]byte(hash), []byte(passwordInput))

	return errHash == nil
}

func generateJWT(userID int) (string, error) {
	secretKey := []byte(config.GetEnv("JWT_KEY"))

	claims := JWTClaims{
		userID,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(1 * time.Hour)),
		},
	}

	jwtInstance := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	jwt, errGenerate := jwtInstance.SignedString(secretKey)
	if errGenerate != nil {
		fmt.Printf("error generating JWT\n %s", errGenerate)
		return "", errGenerate
	}

	return jwt, nil
}

func checkUserExist(userInput UserInput) (int, error) {
	_, errQuery := queryUser(userInput)
	if errQuery != nil {
		if errQuery == sql.ErrNoRows {
			hash, errGenerate := hashPassword(userInput.Password)
			if errGenerate != nil {
				return -1, errors.New("internal error")
			}

			userInput.Password = hash

			userID, errInsert := insertUser(userInput)
			if errInsert != nil {
				return -1, errors.New("internal error")
			}

			return userID, nil
		}
	}

	return -1, errors.New("user exists")
}

func hashPassword(passwordInput string) (string, error) {
	hash, errGenerate := bcrypt.GenerateFromPassword([]byte(passwordInput), 14)
	if errGenerate != nil {
		fmt.Printf("error generating bcrypt hash from password input\n %s", errGenerate)
		return "", errGenerate
	}

	return string(hash), nil
}
