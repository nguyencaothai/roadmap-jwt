package user

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func LoginFunc(w http.ResponseWriter, r *http.Request) {
	var userInput UserInput

	errDecode := json.NewDecoder(r.Body).Decode(&userInput)
	if errDecode != nil {
		fmt.Printf("error decoding user login input\n %s", errDecode)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	jwt, errCheck := checkValidUser(userInput)
	if errCheck != nil {
		if errCheck.Error() == "unauthorized" {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	errEncode := json.NewEncoder(w).Encode(Token{Token: jwt})
	if errEncode != nil {
		fmt.Printf("error encoding token before returning\n %s", errEncode)
	}
}

func RegisterFunc(w http.ResponseWriter, r *http.Request) {
	var userInput UserInput

	errDecode := json.NewDecoder(r.Body).Decode(&userInput)
	if errDecode != nil {
		fmt.Printf("error decoding user login input\n %s", errDecode)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	_, errCheck := checkUserExist(userInput)
	if errCheck != nil {
		if errCheck.Error() == "user exists" {
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}

		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	fmt.Fprintf(w, "OK")
}
