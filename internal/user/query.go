package user

import (
	"fmt"
	"time"

	"local.com/roadmap-JWT/config"
)

func queryUser(userInput UserInput) (User, error) {
	var user User
	query := "select * from users where username = $1"
	errQuery := config.GetDB().QueryRow(query, userInput.Username).Scan(&user.ID, &user.Username, &user.Password, &user.Created_at)
	if errQuery != nil {
		fmt.Printf("error querying user from database\n %s", errQuery)
		return User{}, errQuery
	}

	return user, nil
}

func insertUser(userInput UserInput) (int, error) {
	var userID int
	query := "insert into users(username, password, created_at) values($1, $2, $3) RETURNING id"
	errInsert := config.GetDB().QueryRow(query, userInput.Username, userInput.Password, time.Now()).Scan(&userID)
	if errInsert != nil {
		fmt.Printf("error inserting new user to database\n %s", errInsert)
		return -1, errInsert
	}

	return userID, nil
}
