# Create a new migration<br>

```
migrate create -ext sql -dir migrations [name_of_migration]
```


For example:<br>
```
migrate create -ext sql -dir migrations create_users_table
```

# Make a migration<br>
```
export DB_URL='mysql://username:password@(host:port)/dbname?parseTime=true&multiStatements=true'

migrate  -source file://migrations -database ${DB_URL} up
```

For example:<br>

```
export DB_URL='mysql://root:root@(localhost:3307)/users?parseTime=true&multiStatements=true'

migrate -source file://migrations -database ${DB_URL} up
```

# Build
```
cd roadmap-JWT && go build -o roadmap-JWT cmd/main.go
```

# Run
```
./roadmpa-JWT
```