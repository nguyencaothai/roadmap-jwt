package config

import (
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func RunMigration() error {
	connStr := fmt.Sprintf(
		"%s://%s:%s@%s:%s/%s?sslmode=disable",
		GetEnv("DB_TYPE"),
		GetEnv("DB_USER"),
		GetEnv("DB_PASS"),
		GetEnv("DB_HOST"),
		GetEnv("DB_PORT"),
		GetEnv("DB_NAME"),
	)

	migrateInstance, errInstance := migrate.New("file://migrations", connStr)
	if errInstance != nil {
		return fmt.Errorf("error creating migration instance")
	}

	if errMigrate := migrateInstance.Up(); errMigrate != nil {
		return fmt.Errorf("error running migration")
	}

	return nil
}
