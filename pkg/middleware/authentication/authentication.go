package authentication

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/golang-jwt/jwt/v5"
	"local.com/roadmap-JWT/config"
	"local.com/roadmap-JWT/internal/user"
)

func AuthMiddlware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")

		if authHeader == "" {
			fmt.Printf("error empty value of Authorization header\n")
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}

		token := strings.Split(authHeader, " ")
		if len(token) != 2 || token[0] != "Bearer" {
			fmt.Printf("error wrong format of Authorization header value\n")
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}

		jwt, errParse := jwt.ParseWithClaims(token[1], &user.JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("can not detect signing method")
			}

			return []byte(config.GetEnv("JWT_KEY")), nil
		})

		if errParse != nil {
			fmt.Println("error parsing jwt to claims")
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		claims := jwt.Claims.(*user.JWTClaims)
		ctx := context.WithValue(r.Context(), user.UserIDKey, claims)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
