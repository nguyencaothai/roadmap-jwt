package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"local.com/roadmap-JWT/config"
	"local.com/roadmap-JWT/internal/secret"
	"local.com/roadmap-JWT/internal/user"
	"local.com/roadmap-JWT/pkg/middleware/authentication"
)

func main() {
	errLoadEnv := config.LoadEnv()
	if errLoadEnv != nil {
		fmt.Println(errLoadEnv)
	}

	errDatabaseInit := config.DatabaseInit()
	if errDatabaseInit != nil {
		fmt.Println(errDatabaseInit)
	}

	errMigrate := config.RunMigration()
	if errMigrate != nil {
		fmt.Println(errMigrate)
	}

	r := mux.NewRouter()

	userRoute := r.PathPrefix("/users").Subrouter()
	userRoute.HandleFunc("/register", user.RegisterFunc).Methods("POST")
	userRoute.HandleFunc("/login", user.LoginFunc).Methods("POST")

	secretRoute := r.PathPrefix("/secret").Subrouter()
	secretRoute.Use(authentication.AuthMiddlware)
	secretRoute.HandleFunc("/", secret.SecretFunc).Methods("POST")

	errStart := http.ListenAndServe(":8081", r)
	if errStart != nil {
		fmt.Println("error starting server at port 8081")
	}

}
